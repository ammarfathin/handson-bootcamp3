import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Function;

public class DateFormatter {
    private static String formatDate1(Date someDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY");
        return sdf.format(someDate);
    }
    private static Function<Date, String> formatDate = someDate -> {
        SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY");
        return sdf.format(someDate);
    };
    
    




    public static void main(String[] args) {
        // DateFormatter df = new DateFormatter();
        // Date currentDate = new Date();
        // String formattedDate = df.formatDate(currentDate);
        // System.out.println(formattedDate);
        // 
        // String formattedDate = formatDate.apply(currentDate);
        // System.out.println(formattedDate);

        System.out.println("formatter date with method reference");
        Function<Date, String> formatDate1 = DateFormatter::formatDate1;
        Date currentDate = new Date();
        String formattedDate1 = formatDate1.apply(currentDate);
        System.out.println(formattedDate1);
    }
}
